module "gitlab_ref_arch_azure" {
  source = "../../modules/gitlab_ref_arch_azure"

  prefix               = var.prefix
  location             = var.location
  storage_account_name = var.storage_account_name
  resource_group_name  = var.resource_group_name
  vm_admin_username    = var.vm_admin_username
  ssh_public_key       = file(var.ssh_public_key_file_path)
  external_ip_type     = "Standard"

# Single node with monitor

  ## Rails
  gitlab_rails_node_count = 1
  gitlab_rails_size       = "Standard_F8s_v2"
  # gitlab_rails_external_ip_names = [var.external_ip_name]

  ## Monitor
  monitor_node_count = 1
  monitor_size       = "Standard_F2s_v2"

  haproxy_external_node_count = 1
  haproxy_external_size       = "Standard_F2s_v2"
  haproxy_external_external_ip_names = [var.external_ip_name]
}

output "gitlab_ref_arch_azure" {
  value = module.gitlab_ref_arch_azure
}