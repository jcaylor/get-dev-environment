terraform {
  backend "azurerm" {
    resource_group_name  = "jcaylor"
    storage_account_name = "jcaylordev1"
    container_name       = "jcaylor-dev1-terraform"
    key                  = "jcaylor-dev1.tfstate"
  }
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
  features {}
}
