variable "prefix" {
  default = "dev1"
}

variable "location" {
  default = "East US"
}

variable "resource_group_name" {
  default = "jcaylor"
}

variable "vm_admin_username" {
  default = "jcaylor"
}

variable "ssh_public_key_file_path" {
  default = "/keys/dev1.az.gitlab.caylor.me.pub"
}

variable "storage_account_name" {
  default = "jcaylordev1"
}

variable "external_ip_name" {
  default = "dev1.az.gitlab.caylor.me-public-ip"
}
