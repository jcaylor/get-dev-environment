# The container

## Pre-requisites

- Local directory containing your terraform and ansible configurations for the environment
- SSH KeyPair (RSA) `ssh-keygen -t rsa -b 4096`, public key configured in the terraform `variables.tf` file.
    - The file path in the variables file will be relative to our mount path in the Docker Command section.
    - The docker command section will use the absolute path of the keys on your local system
- Resource Group, Storage Account and Blob Container created for the terraform state, configured in the terraform `main.tf` file.
- Public IP Address Name within the resource group, configured in the terraform `variables.tf` file
- A local custom config file for gitlab_rails and/or other services to enable SAML and other configuration options.
    - This file needs to reside in `dev1/files/gitlab_configs/gitlab_rails.rb.j2`
    - Additional files can reside in `dev1/files/gitlab_configs/` for other services (i.e. `sidekiq.rb.j2`)
    - These are templated files that can contain jinja2 style logic to render variables from our `vars.yaml` file if needed.

### Secrets

Optionally, we can store our secrets into an external secrets manager (Keeper, Vault, KeyVault).
In this example, we have the secrets in a manually created key vault. Reference the `ansible/vars.yaml` file for example.

## Docker command
docker run -it \
    -v ~/.azure:/root/.azure \
    -v ./dev1/ansible:/gitlab-environment-toolkit/ansible/environments/dev1 \
    -v ./dev1/terraform:/gitlab-environment-toolkit/terraform/environments/dev1 \
    -v ./dev1/files:/gitlab-environment-toolkit/ansible/environments/files \
    -v ~/.ssh/dev1.az.gitlab.caylor.me:/keys/dev1.az.gitlab.caylor.me \
    -v ~/.ssh/dev1.az.gitlab.caylor.me.pub:/keys/dev1.az.gitlab.caylor.me.pub \
    registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:3.3.1 

# Running on a non-amd64 system:
docker run --platform linux/amd64 -it \
    -v ~/.azure:/root/.azure \
    -v ./dev1/ansible:/gitlab-environment-toolkit/ansible/environments/dev1 \
    -v ./dev1/files:/gitlab-environment-toolkit/ansible/environments/files \
    -v ./dev1/terraform:/gitlab-environment-toolkit/terraform/environments/dev1 \
    -v ~/.ssh/dev1.az.gitlab.caylor.me:/keys/dev1.az.gitlab.caylor.me \
    -v ~/.ssh/dev1.az.gitlab.caylor.me.pub:/keys/dev1.az.gitlab.caylor.me.pub \
    registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:3.3.1

## Initial Commands

This command installs terraform based on the `.tools-versions` file within the container image (From the GET repository)
`mise install terraform`

This command ensures that this version of terraform is used globally from within the container
`mise use -g terraform`


## Volume Mounts

We have mounted our local azure configuration and the toolkit has the az tool installed. To verify this configuration, we can type `az account show` from within the container.

We have also mounted our `terraform` and `ansible` directories into their respective homes within the toolkit (`/gitlab-environment-toolkit/ansible/environments` and `/gitlab-environment-toolkit/terraform/environments` respectively.) Inside each of these directories within the container, we should see a `dev1` directory.

We have also mounted our SSH private and public keypair into the container in the `/keys` root-level directory for easy absolute path reference.

# Terraform

## Initialization

To begin, lets initialize the terraform module and verify access to the object storage bucket.

```bash
cd /gitlab-environment-toolkit/terraform/environments/dev1
terraform init
```

You should see an output similar to the following:

```
Initializing the backend...
Initializing modules...
- gitlab_ref_arch_azure in ../../modules/gitlab_ref_arch_azure
...
...
Terraform has been successfully initialized!
...
```

This means that we now have a local `.terraform` directory and `.terraform.lock.hcl` file generated within our environments `terraform` directory.
These are the provider and module cache directory and [Dependency Lock File](https://developer.hashicorp.com/terraform/language/files/dependency-lock) 
See [Initializing Working Directories](https://developer.hashicorp.com/terraform/cli/init) for more information.

## Plan

Running a `terraform plan` should now cause terraform to interact with Azure and generate a plan. The output of this command shows you every resource it will be creating. We can use this plan to verify that we are getting the resources we are expecting.

## Apply

Running a `terraform apply` will now re-generate this plan, output the plan to you for validation, and ask if you would like to continue. Typing `y` and hitting enter will cause terraform to begin issuing API calls to azure to create your resources, showing you the status along the way.

Once completed, you should be able to see all of the available resources in your resource group.

### Validation

To validate, we can run the `az` command from outside or inside of the container to list out the resources within our resource group. Alternatively, we can use the web UI for verification.

`az resource list --location "eastus" --resource-group "jcaylor"`


# Ansible

Ansible handles our configuration management of the infrastructure we have built with Terraform. The toolkit contains a set of provided playbooks, roles, and tasks for each gitlab service. This will handle installing, configuring, and starting the services based on the infrastructure tags from the terraform deployment.

```bash
cd /gitlab-environment-toolkit/ansible/
ansible-playbook -i ./environments/dev1 playbooks/all.yml
```

